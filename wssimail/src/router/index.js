import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import GoodLists from '@/views/GoodLists'
import People from '@/views/People'
import Menu from '@/views/Menu'
import Menubanner from '@/views/MenuBanner'
import MenuContent from '@/views/MenuContent'
import Main from '@/views/Main'
import PerInfor from '@/views/PerInfor'
import Header from '@/views/Header'
import Left from '@/views/Left'
import Right from '@/views/Right'
import Footer from '@/views/Footer'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      components:{
        default:HelloWorld,
        header:Header,
        left:Left,
        right:Right,
        footer:Footer
      }
    },
    //动态路由
    {
      path: '/goodlists/:id',
      name:'goodlists',
      component:GoodLists
    },
    {
      path:'/people',
      name:'people',
      component:People
    },
    //------------
    //子路由
    {
      path:'/menu',
      name:'menu',
      component: Menu,
      children:[
        {
          path:'menubanner',
          name:'banner',
          component:Menubanner
        },
        {
          path:'menucontent',
          name:'menucontent',
          component:MenuContent
        }
      ]
    },
    //---------------
    //跳转其他组件
    {
      path:'/main',
      name:'name',
      component:Main
    },
    {
      path:'/perinfor',
      name:'perinfor',
      component:PerInfor
    },
    {
      path:'/header',
      name:'header',
      component:Header
    },
    {
      path:'/left',
      name:'left',
      component:Left
    },
    {
      path:'/right',
      name:'right',
      component:Right
    },
    {
      path:'/footer',
      name:'footer',
      component:Footer
    }
  ]
})
